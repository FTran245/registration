#ifndef REGISTRATION_H
#define REGISTRATION_H
#include "tinyxml2-master\tinyxml2-master\tinyxml2.h"
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <thread>

#include "misc.h"

#include <opencv2\imgcodecs.hpp>
#include "opencv2\imgproc.hpp"
#include "opencv2\highgui.hpp"
#include <opencv2\video.hpp>
#include <opencv2\imgproc.hpp>
#include <opencv2\core\utility.hpp>

using namespace std;
using namespace cv;
using namespace tinyxml2;

class volume
{
public:
	int width;
	int height;
	int Number_of_Frames;
	int Number_of_Volumes;

	int Number_of_BM_scans;
	int ImageSize;

	void getParameters(XMLDocument &doc);

private:
};

// Pass by reference to make it faster
// for some reason the arguments aren't changing color, but it works
void globalReg(vector<Mat> &vol, vector<Mat> &volume_mcorr, int num_iterations, double termination_eps, const int warp_mode); 

void globalRegThread(Mat &src, Mat refer, Mat &dest, int num_iterations, double termination_eps, const int warp_mode, TermCriteria criteria, int num);

void globalRegThreadCall(vector<Mat> &src, Mat ref, vector<Mat> &dest, int num_iterations, double termination_eps, const int warp_mode, TermCriteria criteria, int num_threads, int num);

void localReg(vector<Mat> &vol, vector<Mat> &volume_mcorr, int num_iterations, double termination_eps, const int warp_mode, TermCriteria criteria, int numBM);


/* Phase correlation functions */
void phaseCorrelate(Mat ref, vector<Mat> &volume, vector<Mat> &volume_mcorr);

void callPhaseThread(vector<Mat> &vol, vector<Mat> &volume_mcorr, int numThreads, int p_tid);

void phaseCorrelate_N(Mat fixed, Mat &moving, vector<Mat> &volume_mcorr, float width, float height, int cc);

#endif