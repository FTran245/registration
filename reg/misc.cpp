#include <iostream>
#include <thread>

#include "Registration.h"
#include "tinyxml2-master\tinyxml2-master\tinyxml2.h"

#include <opencv2\imgcodecs.hpp>
#include "opencv2\imgproc.hpp"
#include "opencv2\highgui.hpp"
#include <opencv2\video.hpp>
#include <opencv2\imgproc.hpp>
#include <opencv2\core\utility.hpp>

using namespace std;
using namespace cv;
using namespace tinyxml2;

void dispVolume(vector<Mat> volume)
{
	for (int ii = 0; ii < volume.size(); ii++)
	{
		imshow("volume", volume[ii]);
		waitKey(0.5);
	}
}

void testTryCatch()
{
	//cout << "placehold " << endl;
	int x = 1;
	int y = 1;
	int z;

	try
	{
		z = x / y;
		
	}
	catch(...)
	{
		cout << "error " << y << endl;
	}
	cout << "placehold " << endl;
}

void globalRegTest()
{
	// read in image, it will be RGB by default
	Mat ref, im;
	ref = imread("tempB.tiff");
	im = imread("movB.tiff");

	// Convert to grayscale (BGR in C/C++)
	Mat ref_gr, im_gr;
	cvtColor(ref, ref_gr, CV_BGR2GRAY);
	cvtColor(im, im_gr, CV_BGR2GRAY);

	// check, it works
	/*imshow("ref", ref);
	waitKey(0);*/

	// define the motion model
	const int warp_mode = MOTION_EUCLIDEAN;

	// Set a 2x3 or 3x3 warp matrix dependng on the motion (translational for our testing purposes ..)
	// initialize the matrix to identity (all ones)
	Mat warp_matrix;
	warp_matrix = Mat::eye(2, 3, CV_32F);

	// perhaps set the number of iterations not to be 5000, it'll take too
	int num_iterations = 100;

	// Specify the threshold of increment in the correlation coefficient between two iterations
	double termination_eps = 1e-10;

	// define the termination criteria
	TermCriteria criteria(TermCriteria::COUNT + TermCriteria::EPS, num_iterations, termination_eps);

	// Run the ECC algorithm
	// this is what we've been waiting for
	findTransformECC(ref_gr, im_gr, warp_matrix, warp_mode, criteria);

	// Registered image
	// Use warpAffine for Translational, Euclidean, Affine, use warpPerspective for homography
	Mat reg;
	warpAffine(im_gr, reg, warp_matrix, ref_gr.size(), INTER_LINEAR + WARP_INVERSE_MAP);

	// Display result, looks like it works so far (on intensity scans without filtering like in MATLAB)
	// Let's try to load an entire volume and register it
	imshow("template", ref_gr);
	imshow("reg", reg);
	waitKey(0);

	cout << "placehold" << endl;
}

// https://www.learnopencv.com/image-alignment-ecc-in-opencv-c-python/
void testReg()
{
	// Read in images, they are RGB by default (BGR in C++ for some reason)
	Mat im1, im2;
	im1 = imread("Lena_temp.png");
	im2 = imread("Lena_moving.png");

	// make grayscale
	Mat im1_gr, im2_gr; 
	cvtColor(im1, im1_gr, CV_BGR2GRAY);
	cvtColor(im2, im2_gr, CV_BGR2GRAY);

	/*imshow("im1", im1_gr);
	imshow("im2", im2_gr);
	waitKey(0);
	*/

	// need opencv 3
	// define the motion model
	const int warp_mode = MOTION_EUCLIDEAN;

	// Set a 2x3 or 3x3 warp matrix dependng on the motion (translational for our testing purposes ..)
	// initialize the matrix to identity (all ones)
	Mat warp_matrix;
	warp_matrix = Mat::eye(2, 3, CV_32F);
	//cout << warp_matrix.at<float>(0, 0);

	int num_iterations = 5000;

	// Specify the threshold of increment in the correlation coefficient between two iterations
	double termination_eps = 1e-10;

	// define the termination criteria
	TermCriteria criteria(TermCriteria::COUNT + TermCriteria::EPS, num_iterations, termination_eps);

	// Run the ECC algorithm
	// this is what we've been waiting for
	findTransformECC(im1_gr, im2_gr, warp_matrix, warp_mode, criteria);

	// Registered image
	// Use warpAffine for Translational, Euclidean, Affine, use warpPerspective for homography
	Mat reg;
	warpAffine(im2_gr, reg, warp_matrix, im1_gr.size(), INTER_LINEAR + WARP_INVERSE_MAP);

	// Display result
	imshow("im1", im1_gr);
	imshow("im2", im2_gr);
	imshow("aligned", reg);
	waitKey(0);

}

// Displays Lena in color and grayscale
void Lena()
{
	Mat img = imread("Lenna.png");

	//Vec3b val = img.at<Vec3b>(10, 10);
	//cout << val;

	Mat img_gray; // create grayscale variable

	// mat2gray() for C++ with opencv
	// cvtColor(colorImg, greyscaleImg, mode);
	// Guess this doesn't work? ..
	cvtColor(img, img_gray, cv::COLOR_BGR2GRAY);

	cv::String windowO = "original";
	cv::String windowG = "gray";

	namedWindow(windowO, WINDOW_AUTOSIZE);
	imshow(windowO, img);

	namedWindow(windowG, WINDOW_AUTOSIZE);
	imshow(windowG, img_gray);
	waitKey(0);
}

void tryEstRT()
{
	// Read in images, they are RGB by default (BGR in C++ for some reason)
	Mat im1, im2;
	im1 = imread("Lena_temp.png");
	im2 = imread("Lena_moving.png");

	// make grayscale
	Mat im1_gr, im2_gr;
	cvtColor(im1, im1_gr, CV_BGR2GRAY);
	cvtColor(im2, im2_gr, CV_BGR2GRAY);

	Mat out = Mat::eye(2, 3, CV_32F);
	vector<float> arr;

	out = estimateRigidTransform(im1_gr, im2_gr, false);

	if (out.isContinuous())
	{
		arr.assign(out.data, out.data + out.total());
	}

	cout << out.channels() << "\n";
	cout << out.at<float>(0, 0) << endl;
	//cout << out.at<float>(1, 2) << endl;

	cout << "wait " << endl;
}