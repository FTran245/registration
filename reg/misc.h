#ifndef MISC_H
#define MISC_H

#include <iostream>
#include <thread>

#include "Registration.h"
#include "tinyxml2-master\tinyxml2-master\tinyxml2.h"

#include <opencv2\opencv.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>

using namespace std;
using namespace tinyxml2;
using namespace cv;

void dispVolume(vector<Mat> volume);

void Lena();

void testReg();

void globalRegTest();

void testTryCatch();

void tryEstRT();

#endif