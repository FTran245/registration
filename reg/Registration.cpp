#define _CRT_SECURE_NO_DEPRECATE
#include "tinyxml2-master\tinyxml2-master\tinyxml2.h"

// Built in
#include <iostream>
#include <stdio.h>
#include <iomanip>
#include <fstream>
#include <string>
#include <thread>
#include <chrono>
#include <mutex>

// Library files
#include "Registration.h"
#include "misc.h"

// OpenCV
#include <opencv2\imgcodecs.hpp>
#include "opencv2\imgproc.hpp"
#include "opencv2\highgui.hpp"
#include <opencv2\video.hpp>
#include <opencv2\imgproc.hpp>
#include <opencv2\core\utility.hpp>

using namespace std;
using namespace cv;
using namespace tinyxml2;

mutex mtx;

/* To parse the .xml files of the volumes */
void volume::getParameters(XMLDocument &doc)
{
	//XMLText *widthNode = doc.FirstChildElement("MonsterList")->GetText();
	// const char *test = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Volume_Size")->Attribute("Width");
	// const char *test22 = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Volume_Size")->Attribute("Height");

	width = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Volume_Size")->IntAttribute("Width");
	height = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Volume_Size")->IntAttribute("Height");
	Number_of_Frames = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Volume_Size")->IntAttribute("Number_of_Frames");
	Number_of_Volumes = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Volume_Size")->IntAttribute("Number_of_Volumes");

	ImageSize = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Scanning_Parameters")->IntAttribute("X_Scan_Range");
	Number_of_BM_scans = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Scanning_Parameters")->IntAttribute("X_Scan_Range");
}

/* https://answers.opencv.org/question/1624/phase-correlation-for-image-registrationimage-stitching/ 
Phase correlation - This algorithm works very well, or at least it seems like it 
Actually doesn't work too well for very erratic motion, but for slower motion it should be okay */
void phaseCorrelate(Mat fixed, vector<Mat> &volume, vector<Mat> &volume_mcorr)
{
	//Mat im1 = imread("Lena_temp.png");
	//Mat im2 = imread("Lena_moving.png");
	//cvtColor(im1, im1, CV_BGR2GRAY);
	//cvtColor(im2, im2, CV_BGR2GRAY);

	float width = getOptimalDFTSize(max(fixed.cols, volume[20].cols));
	float height = getOptimalDFTSize(max(fixed.rows, volume[20].rows));

	// initialize matrices
	Mat fft1(Size(width, height), CV_32F, Scalar(0));
	Mat fft2(Size(width, height), CV_32F, Scalar(0));

	// copy images into buffers and fft the reference
	fixed.convertTo(fft1, CV_32F);
	dft(fft1, fft1, 0, fixed.rows);

	// Maximum value of multiplied spectrum and (x, y) offsets
	double maxVal;
	float resX;
	float resY;

	Point maxLoc;
	Mat warp_m = Mat::eye(2, 3, CV_32F);
	Mat im2;

	for (int ii = 0; ii < volume_mcorr.size(); ii++)
	{
		volume[ii].convertTo(fft2, CV_32F);

		// FFT
		dft(fft2, fft2, 0, volume[ii].rows);

		// multiply the spectrums together, then inverse DFT
		mulSpectrums(fft1, fft2, im2, 0, true);
		idft(im2, im2);

		// minMaxLoc finds the global min and max in an array, in our case, the multiplied spectrums
		minMaxLoc(im2, NULL, &maxVal, NULL, &maxLoc);

		// The ?: is a ternary operator, basically goes like this --> int x = condition ? (if true x = n1) : (if false x = n2)
		resX = (maxLoc.x < width / 2) ? (maxLoc.x) : (maxLoc.x - width);
		resY = (maxLoc.y < height / 2) ? (maxLoc.y) : (maxLoc.y - height);

		// We are only interested in the y offset
		warp_m.at<float>(1, 2) = -resY;
		warpAffine(volume[ii], volume_mcorr[ii], warp_m, im2.size(), INTER_LINEAR + WARP_INVERSE_MAP);
	}

	//cout << "placeholding " << endl;
}


void callPhaseThread(vector<Mat> &vol, vector<Mat> &volume_mcorr, int numThreads, int p_tid)
{
	// Create reference
	Mat fixed;
	GaussianBlur(vol[20], fixed, Size(3, 3), 4);

	float width = getOptimalDFTSize(max(fixed.cols, fixed.cols));
	float height = getOptimalDFTSize(max(fixed.rows, fixed.rows));

	for (int cc = p_tid; cc < vol.size(); cc = cc + numThreads)
	{
		phaseCorrelate_N(fixed, vol[cc], volume_mcorr, width, height, cc);
	}
}

/* Multithreaded implementation of phase correlation registration */
void phaseCorrelate_N(Mat fixed, Mat &moving, vector<Mat> &volume_mcorr, float width, float height, int cc)
{
	// initialize matrices
	Mat fft1(Size(width, height), CV_32F, Scalar(0));
	Mat fft2(Size(width, height), CV_32F, Scalar(0));

	// copy images into buffers and fft the reference
	fixed.convertTo(fft1, CV_32F);
	dft(fft1, fft1, 0, fixed.rows);

	// Maximum value of multiplied spectrum and (x, y) offsets
	double maxVal;
	float resX;
	float resY;

	Point maxLoc;
	Mat warp_m = Mat::eye(2, 3, CV_32F);
	Mat im2;

	// change to 32 bit float
	moving.convertTo(fft2, CV_32F);

	// FFT
	dft(fft2, fft2, 0, moving.rows);

	// multiply the spectrums together - the boolean flag specifics if complex conjugate the second spectrum, then inverse DFT
	mulSpectrums(fft1, fft2, im2, 0, true);
	idft(im2, im2);

	// minMaxLoc finds the global min and max in an array, in our case, the multiplied spectrums
	minMaxLoc(im2, NULL, &maxVal, NULL, &maxLoc);

	// The ?: is a ternary operator, basically goes like this --> int x = condition ? (if true x = n1) : (if false x = n2)
	resX = (maxLoc.x < width / 2) ? (maxLoc.x) : (maxLoc.x - width);
	resY = (maxLoc.y < height / 2) ? (maxLoc.y) : (maxLoc.y - height);

	// Apply the translation - we are only interested in the y offset
	warp_m.at<float>(1, 2) = -resY;
	warpAffine(moving, volume_mcorr[cc], warp_m, im2.size(), INTER_LINEAR + WARP_INVERSE_MAP);
}

/* Global registration on a reference frame */
void globalReg(vector<Mat> &vol, vector<Mat> &volume_mcorr, int num_iterations, double termination_eps, const int warp_mode)
{
	// Pick a reference frame to register to
	Mat ref;
	ref = vol[20];

	// define the termination criteria
	TermCriteria criteria(TermCriteria::COUNT + TermCriteria::EPS, num_iterations, termination_eps);

	// ========== This is the motion correction part ========== //
	// Run the ECC algorithm
	// this is what we've been waiting for

	vector<float> y_offsets(vol.size());

	// Set a 2x3 or 3x3 warp matrix dependng on the motion (translational for our testing purposes ..)
	// initialize the matrix to identity (all ones)
	Mat warp_matrix;
	warp_matrix = Mat::eye(2, 3, CV_32F);

	//findTransformECC(ref, vol[34], warp_matrix, warp_mode, criteria);
	for (int jj = 0; jj < vol.size(); jj++)
	{
		//myThreads[jj] = thread(globalRegThread, vol[jj], volume_mcorr[jj], warp_matrix, num_iterations, termination_eps, warp_mode, criteria, jj);
		cout << "Iteration: " << jj << endl;
		
		try
		{
			// Find the rigid deformation using findTransformECC
			findTransformECC(ref, vol[jj], warp_matrix, warp_mode, criteria);

			//Apply the transform. Use warpAffine for Translational, Euclidean, Affine,
			//use warpPerspective for homography. store the registered image in a temporary
			//variable and push back to the mcorr vector 
			//https://docs.opencv.org/2.4/modules/imgproc/doc/geometric_transformations.html?highlight=warpaffine#void%20warpAffine(InputArray%20src,%20OutputArray%20dst,%20InputArray%20M,%20Size%20dsize,%20int%20flags,%20int%20borderMode,%20const%20Scalar&%20borderValue)
			// cout << warp_matrix.at<float>(0, 0) << " "<< warp_matrix.at<float>(0, 1) << warp_matrix.at<float>(0, 2) << endl;
			// cout << warp_matrix.at<float>(1, 0) << " " << warp_matrix.at<float>(1, 1) << warp_matrix.at<float>(1, 2) << endl;
			// Set the x translation to 0
			warp_matrix.at<float>(0, 2) = 0;
			y_offsets.push_back(warp_matrix.at<float>(1, 2));

			warpAffine(vol[jj], volume_mcorr[jj], warp_matrix, ref.size(), INTER_LINEAR + WARP_INVERSE_MAP);
		}
		catch(...) // Catch all expcetions that we don't know about
		{
			//cout << "Error frame: " << jj << endl;
			volume_mcorr[jj] = vol[jj];
			y_offsets.push_back(warp_matrix.at<float>(1, 2));
		}
	}
	cout << "wait \n";
}

/* Thread function */
void globalRegThread(Mat &src, Mat refer, Mat &dest, int num_iterations, double termination_eps, const int warp_mode, TermCriteria criteria, int num)
{
	// Create identity matrix for translation, increases run time by 40 seconds
	Mat warp_m;
	warp_m = Mat::eye(2, 3, CV_32F);

	try
	{
		// Find the rigid deformation using findTransformECC
		findTransformECC(refer, src, warp_m, warp_mode, criteria);

		// Set x translation to 0, only y translation allowed
		warp_m.at<float>(0, 2) = 0;
		warpAffine(src, dest, warp_m, src.size(), INTER_LINEAR + WARP_INVERSE_MAP);
	}
	catch (...) // Catch all expcetions that we don't know about
	{
		//cout << "Error frame: " << num << endl;
		dest = src;
	}
}

void globalRegThreadCall(vector<Mat> &src, Mat ref, vector<Mat> &dest, int num_iterations, double termination_eps, const int warp_mode, TermCriteria criteria, int num_threads, int num)
{
	// Have each thread skip num_threads number of frames, so they work in parallel without bumping into each other
	for (int c = num; c < dest.size(); c = c + num_threads)
	{
		globalRegThread(src[c], ref, dest[c], num_iterations, termination_eps, warp_mode, criteria, num);
	}
}

/* Local registration, fails horribly for some reason ... */
void localReg(vector<Mat> &vol, vector<Mat> &volume_mcorr, int num_iterations, double termination_eps, const int warp_mode, TermCriteria criteria, int numBM)
{
	int numFr = vol.size();

	// Create identity matrix for translation, increases run time by 40 seconds
	Mat warp_matrix;
	warp_matrix = Mat::eye(2, 3, CV_32F);

	for (int i = 0; i < numFr; i = i + numBM)
	{
		for (int j = i; j < i + numBM; j++)
		{
			try
			{
				findTransformECC(vol[i], vol[j], warp_matrix, warp_mode, criteria);
				warp_matrix.at<float>(0, 2) = 0;
				warpAffine(vol[j], volume_mcorr[j], warp_matrix, vol[20].size(), INTER_LINEAR + WARP_INVERSE_MAP);
			}
			catch (...)
			{
				volume_mcorr[j] = vol[j];
			}
		}
	}
}