#include "tinyxml2-master\tinyxml2-master\tinyxml2.h" // the order matters!
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <thread>
#include <chrono>

// Self defined
#include "misc.h"
#include "Registration.h"
#include "BVsmooth3D.h"

//openCV
#include <opencv2\imgcodecs.hpp>
#include "opencv2\imgproc.hpp"
#include "opencv2\highgui.hpp"
#include <opencv2\video.hpp>
#include <opencv2\imgproc.hpp>
#include <opencv2\core\utility.hpp>

using namespace std;
using namespace cv;
using namespace tinyxml2;

int main()
{
	// used this for opencv3 https://www.youtube.com/watch?v=ldds8oXVOyI

	// declare object and set filename and load the xml into XML object doc
	volume V1;
	const char* fn_xml = "9_28_11-H932-OD-FOV.xml";
	XMLDocument doc;
	doc.LoadFile(fn_xml);

	// set the parameters (getParameters from pipeline)
	V1.getParameters(doc);

	/*The first step is to find out how to read in .mat files in C/C++
	Perhaps an easy way to start this is to find out how to read in .tiff stacks instead.
	Apparently there's a MATLAB API to do this already but I can't find it anywhere
	Turns out I'm going to be lazy and read in several hundred .tiff images like Fai does */

	vector<Mat> vol;
	vector<cv::String> fn;
	string path;
	path = "C:/Users/Francis/Desktop/Code/reg/reg/movingvolume/*.tiff";

	glob(path, fn, false); // find all files with .tiff extension and place them in vector fn

	// Read in images to "volume" in grayscale
	// We need to find a way to load in volumes in a particular sequence
	int ii = 0;
	for (ii = 0; ii < fn.size(); ii++)
	{
		vol.push_back(imread(fn[ii], CV_LOAD_IMAGE_GRAYSCALE));
	}

	/* BVsmooth function call */

	BVsmooth3D(vol);





	vector<Mat> volume_MCORR(vol.size());

	/*
	single threaded implementation - 19,070,672 us = 19.07 seconds for 1600 frames
	Mat fixed;
	fixed = vol[20];
	// timer
	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	phaseCorrelate(fixed, vol, volume_MCORR);
	// timer
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << endl;
	*/

	const int phase_threads = 6;
	thread phaseThreads[phase_threads];

	/* multi-threaded phase correlation motion correction 6 threads, 3,670,081 us = 3.67 seconds for 500 frames, 8.4 seconds for 1600 frames
	This is super fast */
	// timer
	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	for (int kk = 0; kk < phase_threads; kk++)
	{
		phaseThreads[kk] = thread(callPhaseThread, ref(vol), ref(volume_MCORR), phase_threads, kk);
	}

	for (int fin = 0; fin < phase_threads; fin++)
	{
		phaseThreads[fin].join();
	}
	// timer
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << endl;

	dispVolume(volume_MCORR);


	/* I don't think globalReg() actually works, all the offsets turned out to be 0
	That's okay because phase correlation works */

	// initialize output
	vector<Mat> volume_mcorr(vol.size());

	/* findTransformECC algorithm */
	// Set the number of iterations, maybe less than 5000 so it doesn't take so long
	// Specify the threshold of increment in the correlation coefficient between two iterations
	// Also define the motion model
	int num_iterations = 2;
	double termination_eps = 1e-6;
	const int warp_mode = MOTION_TRANSLATION;

	// Set a 2x3 or 3x3 warp matrix dependng on the motion (translational for our testing purposes ..)
	// initialize the matrix to identity (all ones)
	Mat warp_matrix;
	warp_matrix = Mat::eye(2, 3, CV_32F);

	// define the termination criteria
	TermCriteria criteria(TermCriteria::COUNT + TermCriteria::EPS, num_iterations, termination_eps);

	
	// timer
	//std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

	// globalReg(source, dest, warp_matrix, num_iterations, termination, warp_mode);
	// single threaded = 279,491,749 us = 279.49 seconds = ~5 mins
	// globalReg(vol, volume_mcorr, num_iterations, termination_eps, warp_mode);

	// timer
	//std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

	//cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << endl;

	/*
	// Pick a reference frame to register to
	Mat refer;
	GaussianBlur(vol[20], refer, Size(3, 3), 4);
	//refer = vol[20];

	const int numThreads = 6;
	thread myThreads[numThreads];

	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

	// Run multithreaded registration
	// 97, 367, 598 us = 97 seconds = ~1.5 mins
	// 131, 559, 426 us with locks ..but again without locks, maybe too many programs open right now
	// Could be because we initialize the warp matrix in the thread function ..
	// 6 threads = 107, 705, 871 us = 107 seconds
	for (int tid = 0; tid < numThreads; tid++)
	{
		myThreads[tid] = thread(globalRegThreadCall, ref(vol), refer, ref(volume_mcorr), num_iterations, termination_eps, warp_mode, criteria, numThreads, tid);
	}

	for (int tid = 0; tid < numThreads; tid++)
	{
		myThreads[tid].join();
	}

	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << endl;

	
	// Check the volume
	for (int sh = 0; sh < volume_mcorr.size(); sh++)
	{
		imshow("vMcorr ", volume_mcorr[sh]);
		waitKey(0.5);
	}
	*/

	/* MATLAB globalReg() takes 28.04 seconds, we've gotta improve our algorithm */
	/* localReg fails horribly. Doesn't seem to work like that with findTransformECC */
	/*cout << "Placehold " << endl;
	int numBM = 4;
	localReg(vol, volume_mcorr, num_iterations, termination_eps, warp_mode, criteria, numBM);
	*/

 	return 0;
}